# Trouble shooting.
* `TypeError: include() got an unexpected keyword argument 'app_name'`

   Using `app_name` with `include` is deprecated in Django 1.9 and does not work in Django 2.0. Set `app_name` in `blog/urls.py` instead.
* [Type object 'Post' has no attribute 'published' Django](https://stackoverflow.com/questions/42397422/django-blog-posts-not-being-returned)
*  [Django 1.10 doesn't show post from db](https://stackoverflow.com/questions/41246907/django-1-10-doesnt-show-post-from-db)

   Please try to create an Minimal, Complete, and Verifiable example, its pretty clear that your question has nothing to do with a PublishManager class, or the url patterns for admin etc
* [RichTextField ckeditor, post is source code](https://stackoverflow.com/a/45696630/7583919)

  From this problem, I learn don't rush to create an issue then wait for an answer, be patient, analyze the problem, better can simplify it to a problem what you have an idea.

* when the rendering is not working properly which the html source code is displayed directly
  
  `{{ post.body}}` to `{{ post.body | safe }}` 
# Techniques
Develop two version of the project, stable and developing. This can prevent you
from mess the project up with a little mistake which you can hardly find out.
Moreover, develop the project in 2 separate repository goes against the spirit of Git.
```sh
git branch wizard
git checkout wizard #switch to branch wizard
# After Developing
git checkout master
git merge wizard
```

# Reference
* [https://django.readthedocs.io/en/2.1.x/topics/http/urls.html](https://django.readthedocs.io/en/2.1.x/topics/http/urls.html)
* Django 2 by example by ANTONIO MELÉ

